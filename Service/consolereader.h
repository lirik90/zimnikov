#ifndef CONSOLEREADER_H
#define CONSOLEREADER_H

#include <QSocketNotifier>

namespace Zimnikov {

class ConsoleReader : public QObject
{
    Q_OBJECT
public:
    explicit ConsoleReader(QObject *parent = 0);
signals:
    void textReceived(const QString& message);
public slots:
    void text();
private:
    QSocketNotifier notifier;
};

} // namespace Zimnikov

#endif // CONSOLEREADER_H
