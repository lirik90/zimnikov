#ifndef ZIMNIKOV_DTLS_SERVERNODE_H
#define ZIMNIKOV_DTLS_SERVERNODE_H

#include <botan/tls_server.h>
#include <Zimnikov/dtlsproto.h>

namespace Zimnikov {
namespace DTLS {

class ServerNode : public ProtoTemplate<Botan::TLS::Server, bool/*is_datagram*/>
{
    Q_OBJECT
public:
    explicit ServerNode();
    ~ServerNode();
};

} // namespace DTLS
} // namespace Zimnikov

#endif // ZIMNIKOV_DTLS_SERVERNODE_H
