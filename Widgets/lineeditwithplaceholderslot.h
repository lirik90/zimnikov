#ifndef LINEEDITWITHPLACEHOLDERSLOT_H
#define LINEEDITWITHPLACEHOLDERSLOT_H

#include <QLineEdit>

namespace Zimnikov {

class LineEditWithPlaceHolderSlot : public QLineEdit
{
    Q_OBJECT
public:
    using QLineEdit::QLineEdit;
public slots:
    void setPlaceholderText(const QString& text);
};

} // namespace Zimnikov

#endif // LINEEDITWITHPLACEHOLDERSLOT_H
