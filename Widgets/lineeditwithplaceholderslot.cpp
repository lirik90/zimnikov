#include "lineeditwithplaceholderslot.h"

namespace Zimnikov {

void LineEditWithPlaceHolderSlot::setPlaceholderText(const QString &text)
{
    QLineEdit::setPlaceholderText(text);
}

} // namespace Zimnikov
