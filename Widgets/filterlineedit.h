#ifndef FILTERLINEEDIT_H
#define FILTERLINEEDIT_H

#include <QLineEdit>

class QAction;

namespace Zimnikov {

class FilterLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    FilterLineEdit(QWidget* parent = 0);
signals:
    void caseSensitiv(bool);
    void fixedString(bool);
private slots:
    void filterChanged(bool b);
private:
    QAction *csAction;
};

} // namespace Zimnikov

#endif // FILTERLINEEDIT_H
