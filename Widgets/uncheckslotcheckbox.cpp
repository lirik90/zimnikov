#include "uncheckslotcheckbox.h"

namespace Zimnikov {

void UnCheckSlotCheckBox::setDefaultCheckState(Qt::CheckState state)
{
    defaultCheckState = state;
}

void UnCheckSlotCheckBox::unCheck()
{
    setCheckState( defaultCheckState );
}

} // namespace Zimnikov

