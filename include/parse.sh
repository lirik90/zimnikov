#!/bin/bash

LOCKFILE="/tmp/`basename $0`.lock"

[ -f $LOCKFILE ] && exit 0
touch $LOCKFILE

FIND_PATH=$1
if [ -z "$1" ]; then
    FIND_PATH=../
fi

PATH_TO=$2
if [ -z "$2" ]; then
    PATH_TO=.
fi

[ -d $PATH_TO/Zimnikov ] || mkdir -p $PATH_TO/Zimnikov
rm -f $PATH_TO/Zimnikov/* || true
find $FIND_PATH -name "*.h" ! -path "$FIND_PATH/include/Zimnikov/*" -exec ln -s ../{} $PATH_TO/Zimnikov/ \;

sleep 3
rm -f $LOCKFILE

exit 0
